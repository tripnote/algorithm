## 문제 - 목표량

2024/07/08

엘리스 토끼는 목표량을 정해 수학 문제를 열심히 풉니다. 목표량은 정수입니다.

내일 풀 수학 문제의 개수는 오늘 푼 문제 개수의 수와 숫자의 구성이 같으면서, 오늘 푼 문제 개수의 수보다 큰 수 중 가장 작은 수입니다.

예를 들어, 오늘 67문제를 풀었으면 다음 날 76문제를 풉니다.

오늘 푼 문제의 개수를 줬을 때 다음날 풀 문제의 개수를 출력하는 프로그램을 작성하세요.

시간 제한: 1초

---
#### 입력

첫 번째 줄에 오늘 푼 문제의 개수인 자연수 N을 입력합니다.

1≤N≤999999

정답이 반드시 있는 경우만 입력값으로 주어집니다.

```java
//입력예시
364
```
#### 출력
다음날 풀 문제의 개수를 출력합니다.

```java
//출력예시
436
```
---

### 풀이방법 1. 순열

주어진 수로 만들 수 있는 모든 수들을 찾아 리스트로 만들고 

이를 오름차순으로 정렬한 뒤 

주어진 수 다음에 오는 수를 출력한다.

```java
import java.util.*;

class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //입력받은 수
        int num = sc.nextInt();
        String numstr = Integer.toString(num);
        int numLength = numstr.length();
        int[] numArr = new int[numLength];

        for(int i = 0; i < numLength; i++){
            numArr[i] = numstr.charAt(i) - '0';
        }


        //새로 생성될 수
        int[] newNum = new int[numLength];
        //생성된 수들의 리스트
        ArrayList<Integer> newNumbers = new ArrayList<>();

        //중복을 제거하기 위해 방문했는지를 체크하는 배열
        boolean[] visited = new boolean[numLength];

        //입력 받은 숫자의 수와 구성이 같은 수들을 생성 및 배열에 추가
        per(numArr, newNum, visited, 0, numLength, newNumbers);

        //생성된 수들의 리스트를 오름차순으로 정렬
        Collections.sort(newNumbers);

        //입력받은 수보다 큰 값 중 가장 작은 값
        for(int n: newNumbers){
            if(n > num){
                System.out.println(n);
                break;
            }
        }

    }

    //arr: 입력받은 수, output: 생성된 수, visited: 방문여부, depth: 깊이, n: 입력받은 수의 길이, newNumbers: 새로 생성된 수들의 리스트
    static void per(int[] arr, int[] output, boolean[] visited, int depth, int n, ArrayList<Integer> newNumbers){
        if(depth == n){
            int outputNum = arrayToInt(output);
            newNumbers.add(outputNum);
            return;
        }

        for(int i = 0; i < n; i++){
            if(!visited[i]){
                visited[i] = true;
                output[depth] = arr[i];
                per(arr, output, visited, depth +1, n, newNumbers);
                visited[i] = false;
            }
        }
    }

    //int배열을 int로
    //{1,2,3} -> 123
    static int arrayToInt(int[] arr){
        int num = 0;
        for (int digit : arr){
            num = num * 10 + digit;
        }
        return num;
    }
}
```

### 풀이방법2.

주어진 수를 배열로 변환하고 ```ex) 123 -> {1, 2, 3}```  각각의 요소에서 뒤에 있는 요소 중 자신보다 큰 수인 요소가 있을 때,

그 중 가장 뒤에 있는 요소를 선택한다.

이후 선택된 요소보다 뒤에 있으며 선택된 요소보다 큰 수인 요소 중 가장 작은 수를 찾고 이를 선택된 요소와 변경한다.

**선택된 요소보다 뒤에 있는 수 중 가장 작은 수는 가장 뒤에 있는 수이다.*

마지막으로 선택된 요소의 뒤에 오는 요소들을 오름차순으로 정렬한다.

##### 예시1

```8 6 2 4 7``` 라는 수가 주어졌을 때 각각의 자리에있는 수보다 뒤에 더 큰 수가 있는 지 찾아보면
    
```X 7 4 7 X``` 이다.

이 경우의 출력은 ```8 6 2 7 4``` 로 뒤에 더 큰 수가 존재하는 수 중 가장 작은 자리수인 4가 7이 되면 원하는 수가 된다.

##### 예시2

```2 8 3 4 6``` 의 경우

```3 X 4 6 X```

이 경우의 출력은 ```2 8 3 6 4```로 뒤에 더 큰 수가 존재하는 수 중 가장 작은 자리수인 4가 7이 되면 원하는 수가 된다.

##### 예시3

```6 0 2 4 3``` 의 경우

```X 2 3 X X```

이 경우의 출력은 ```6 0 3 2 4```로 뒤에 더 큰 수가 존재하는 수 중 가장 작은 자리수인 2가 3이 되고 그 뒤의 수는 오름차순 정렬을 하면 원하는 수가 된다.

##### 예시4

```6 0 7 4 2```의 경우

```7 2 X X X```

이 경우의 출력은 ```6 2 0 4 7```로 뒤에 더 큰 수가 존재하는 수 중 가장 작은 자리수인 0이 2가 되고 그 뒤의 수는 오름차순 정렬을 하면 원하는 수가 된다.



```java
import java.util.*;

class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //입력받은 수
        String s = sc.next();
        char[] charArr = s.toCharArray();
        int arrLength = charArr.length;

        int select = -1;

        //뒤에서부터 해당 요소보다 뒤에 더 큰 숫자가 있는 요소를 찾고 그 위치를 select에 저장한다.
        for (int i = arrLength - 2; i >= 0; i--) {
            if (charArr[i] < charArr[i + 1]) {
                select = i;
                break;
            }
        }
        
        //select == -1 인 경우 주어진 수가 내림차순(가장 큰 수)이므로 그대로 출력 
        if(select != -1){
            //변경될 값은 선택된 요소의 뒷 배열 요소여야 하며 그 중 가장 작은 수여야 한다.
            int change = arrLength-1;  //뒷 배열 요소 중 가장 작은 수

            //선택된 요소보다 크고 가장 작은 수
            for(int i = arrLength - 1; i > select; i--){
                if(charArr[select] < charArr[i]){
                    change = i;
                    break;
                }
            }

            //선택된 요소와 변경될 요소를 변경
            char item = charArr[select];
            charArr[select] = charArr[change];
            charArr[change] = item;

            //선택된 요소의 뒤에 오는 요소들을 오름차순으로 정렬
            Arrays.sort(charArr, select + 1, arrLength);
        }

        System.out.print(new String(charArr));
    }

}
```
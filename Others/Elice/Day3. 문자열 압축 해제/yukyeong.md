## 문제 - 문자열 압축 해제

2024/07/12

엘리스 토끼는 문자열을 직접 압축 해제하려고 합니다.

압축되지 않은 문자열 S가 주어졌을 때, 이 문자열 중 어떤 부분 문자열은 K(Q)와 같이 압축할 수 있습니다. 이것은 Q라는 문자열이 K 번 반복된다는 뜻입니다. K는 한 자릿수의 정수이고, Q는 0자리 이상의 문자열입니다.

예를 들면, 53(8)은 다음과 같이 압축을 해제할 수 있습니다.

53(8) = 5 + 3(8) = 5 + 888 = 5888

압축된 문자열이 주어졌을 때, 이 문자열을 다시 압축을 푸는 프로그램을 작성하세요.

시간 제한: 1초

---
#### 입력

첫째 줄에 압축된 문자열 S를 입력합니다.

S의 길이는 최대 50입니다.

문자열은 (, ), 숫자로만 구성되어 있습니다.

```java
//입력예시
11(18(72(7)))
```
#### 출력
압축되지 않은 문자열의 길이를 출력합니다.

```java
//출력예시
26
```
---

### 풀이방법1. 노가다..
```11(18(72(7)))```라는 입력이 있을 때, ```2(7) = "777"``` -> ```"7" +"777" = "7777"``` -> ```8(7777) = "7777"*8``` 이런식으로 뒤에서 부터 문자열의 압축을 푼다.

이를 이용해 첫 문자열의 길이를 answer에 저장한 후 뒤에서부터 '(' 의 위치를 찾고 다음에 오는 '(' 와의 사이에 있는 문자의 개수별로 조건을 생성하여 문자열의 길이를 찾는다.

```java
import java.util.*;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        char[] arr = str.toCharArray();
        int n = arr.length;

        //문자열의 길이
        int answer = 0;
        //가장 뒤에 있는 문자의 위치
        int end = -1;
        int start = -1;
        int firstcnt = 0;

        //'('가 있는 위치
        int loc = -1;

        int last = 0;
        int lastcnt = 0;

        //올바른 문자열인지 체크
        int a = 0;
        int b = 0;
        for(int i = 0; i < n; i++){
            if(arr[i] == ')'){
                a++;
            }
            if(arr[i] == '('){
                b++;
            }
        }
        if(a != b){
            System.out.println(n);
        }else{
            //괄호가 없는 문자열인 경우
            if(a == 0){
                System.out.println(n);
                return;
            }
            for(int i = n-1; i >= 0; i--){
                //마지막 ')' 뒤에 문자열이 있는 경우
                if(arr[i] == ')'){
                    if(lastcnt == 0){
                        if(i != n-1){
                            last = n - 1 - i;
                        }
                        lastcnt++;
                    }
                    end = i - 1;
                }
                if(arr[i] == '(' || i == 0){

                    if(firstcnt == 0){
                        loc = i;
                        start = i + 1;
                        //첫번째 문자열의 길이
                        answer = end - start + 1;
                        firstcnt++;
                    }else{

                        start = i;
                        //다음 문자열의 길이
                        int strlength = loc - start - 1;
                        if(arr[0] != '(' && i == 0){
                            strlength += 1;
                        }
                        if(strlength == 0){  //((s))
                            continue;
                        }else if(strlength == 1){
                            answer = (arr[loc-1] - '0')*answer;
                            loc = i;
                        }else{
                            //K 앞의 숫자
                            strlength -= 1;
                            answer = strlength + (arr[loc-1] - '0')*answer;
                            loc = i;
                        }
                    }
                }
            }
            System.out.println(answer + last);
        }
    }
}
```

### 풀이방법2. 스택(Stack)
1번과 비교해서 stack을 활용하여 괄호의 유효성을 검증하는 로직을 추가하였고

압축 해제하는 로직을 좀 더 단순하게 정리하였다.

```java
import java.util.*;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        Stack<Character> stack = new Stack<>();

        for(char c: str.toCharArray()){
            stack.push(c);
        }

        //괄호가 올바르지 않을 경우 입력받은 문자열의 길이 출력
        if(!isValid(str)){
            System.out.println(stack.size());
            return;
        }

        System.out.println(unzip(stack));

    }

    //괄호가 올바른지 확인
    static boolean isValid(String str){
        Stack<Character> stack = new Stack<>();

        for(char c: str.toCharArray()){
            if(c == '('){
                stack.push(c);
            }
            if(c == ')'){
                if(stack.isEmpty() || stack.pop() != '('){
                    return false;
                }
            }
        }
        return true;
    }

    static int unzip(Stack<Character> stack){
        int result = 0;
        Stack<Integer> save = new Stack<>();
        Stack<Integer> mul = new Stack<>();

        //스택이 빌 때까지
        while(!stack.isEmpty()){
            char c = stack.pop();  //위에서부터 하나씩 꺼내기

            if(Character.isDigit(c)){
                if(mul.isEmpty()){
                    result++;
                }else{
                    result = mul.pop() * (c - '0');
                }
            }else if(c == ')'){
                save.push(result);
                result = 0;  //result 0으로 초기화
            }else if(c == '('){
                mul.push(result);   //'(' 가 나오면 ()사이에 존재하는 문자의 길이를 mul에 저장
                result = 0;  //result 0으로 초기화
            }
        }

        //최종 길이 합
        //result + save에 저장된 값들의 합
        while(!save.isEmpty()){
            result += save.pop();
        }

        return result;
    }
}
```

#문제(https://leetcode.com/problems/is-array-a-preorder-of-some-binary-tree/description/)

2024/07/20

중급

푸는 데 걸린 시간: 2시간 

주어진 행렬로 binary구조를 만들 때 preorder search를 시행한 것인지를 묻는 질문입니다. 

(즉, 동일한 tree를 만들어도 순서가 다르면  틀린 것입니다.) 

처음에는 List<List<Integer>> 로 주어진 행렬 만으로 문제를 풀려고 하다가 잘 안 되서 tree 클래스를 만들고 조건을 넣었습니다. 

저보다 2배 빠른 솔루션이 있어서 봤더니 역시 트리를 만들지 않고 그냥 풀더군요. 

아마 그렇게 하나 저처럼 하나 시간복잡도는 O(N logN) 일 것입니다만
(N: 원소 순회, logN : 뿌리에서 루트로 올라가는 알고리즘 때문에 길이 logN이 추가됨)

overhead 때문에 시간이 차이나는 것 같습니다.

```java
class Solution {
    class Tree{
        Tree parent;
        int value;
        Tree left = null;
        Tree right = null;
        public Tree(int value, Tree parent){
            this.value = value;
            this.parent = parent;
        }
    }
    public boolean isPreorder(List<List<Integer>> nodes) {


        Iterator<List<Integer>> node = nodes.iterator();
        List<Integer> nodeCurrent = node.next();
        if(nodeCurrent.get(1) != -1){
            return false;
        }
        Tree currentTree = new Tree(nodeCurrent.get(0), null);
        while(node.hasNext()){

            nodeCurrent = node.next();

            while(currentTree != null){

                if (currentTree.value == nodeCurrent.get(1)){
                    break;
                    
                }
                currentTree = currentTree.parent;
            }
            if(currentTree == null){
                return false;
            }

            currentTree = new Tree(nodeCurrent.get(0), currentTree);
            if(currentTree.parent.left != null){
                currentTree.parent.right = currentTree;
                continue;
            }
            currentTree.parent.left = currentTree;

            if(!isAcceptable(currentTree)){
                return false;
            }


        }

        return true;
        
    }


    private boolean isAcceptable(Tree tree){
        Tree currentTree = tree;
        while(currentTree.parent !=null){
            if(currentTree.parent.left == currentTree && currentTree.parent.right !=null){
                return false;
            }
            currentTree = currentTree.parent;
        }
        return true;
    }
}
```
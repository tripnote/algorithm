#문제(https://leetcode.com/problems/friday-purchase-iii/description/)

2024/07/18

중급

걸린 시간 1시간 30분

문제 자체 한달 간 매주 금요일마다 특정 계급의 유저들이 총 얼마의 돈을 썼는지를 묻는 문제로 문제 자체는 어렵지 않..은데

테이블을 생성하는 것이 어렵습니다.

예를 들어 week_of_month를 date로부터 계산할 수 있어야 하고 실제 테이블에 금요일이 포함되어 있지 않아도 

결과에는 금요일에 관련된 행이 있어야 합니다.(물론 돈은 0이겠지요)

꼼수일지는 모르겠지만 처음부터 day_of_week와 membership에 대해 모두 있는(그래서 총 4*2 = 8개의 행) 테이블을

 미리 만들고 그 테이블을 가지고 계산을 하면 쉽습니다.

```sql
WITH week_numbers AS(
    SELECT 1 AS week_of_month
    UNION ALL SELECT 2
    UNION ALL SELECT 3
    UNION ALL SELECT 4
),
grade_date AS(
    SELECT week_of_month, membership
    FROM week_numbers 
    JOIN Users
    WHERE membership != 'Standard'
    GROUP BY week_of_month, membership
)
SELECT grade_date.week_of_month AS week_of_month, grade_date.membership AS membership, COALESCE(sum(p1.amount_spend), 0) AS total_amount
FROM grade_date
LEFT JOIN (
    SELECT u1.membership, p.purchase_date, p.amount_spend
    FROM Purchases p
    INNER JOIN Users u1
    ON u1.user_id = p.user_id
    WHERE DAYOFWEEK(p.purchase_date) = 6
    AND
    u1.membership != 'Standard'
) AS p1
ON FLOOR((DayOfMonth(p1.purchase_date)-1)/7)+1 = grade_date.week_of_month
AND p1.membership = grade_date.membership
GROUP BY week_of_month, membership
ORDER BY week_of_month;
```
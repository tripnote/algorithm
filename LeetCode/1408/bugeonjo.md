#문제(https://leetcode.com/problems/string-matching-in-an-array/description/)

2024/07/16

초급

걸린 시간: 20분

스트링의 서브 스트링을 구하는 문제는 단골 문제입니다.

저는 각 스트링들을 char[]로 변환하여 각 character들을 비교하는 가장 단순한 방식을 사용했는데, 

가장 빠른 사람 코드를 보니까 string1.contains(string2)를 체크하는 것이 가장 빠르네요. 

이쪽 알고리즘도 많은 걸로 알고 있는데 공부하는 것이 좋을 것 같습니다.

```java
class Solution {
    public List<String> stringMatching(String[] words) {
        List<String> answer = new ArrayList<>();

        Arrays.sort(words, Comparator.comparingInt(String::length));

        for(int i = 0; i < words.length; i++){
            for(int j = i+1; j < words.length; j++){
                if(isSubstring(words[i],words[j])){
                    answer.add(words[i]);
                    break;

                }
            }
        }
        return answer;
    }

    private boolean isSubstring(String subword, String word){
        char[] subwordCha = subword.toCharArray();
        char[] wordCha = word.toCharArray();
        boolean isMatched;
        for(int i = 0; i < wordCha.length - subwordCha.length + 1; i++ ){
            isMatched = true;
            for(int j =0; j < subwordCha.length; j++){
                if(wordCha[i + j] != subwordCha[j]){
                    isMatched = false;
                    break;
                }

            }
            if(isMatched){
                return true;
            }
        }
        return false;
    }
}
```
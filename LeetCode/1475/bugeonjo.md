#문제(https://leetcode.com/problems/final-prices-with-a-special-discount-in-a-shop/)

2024/07/17

초급

걸린 시간: 7분

아주 간단한 문제입니다. 핵심은 arr[i]보다 작은 값이 i+1부터 나타나냐는 것입니다. 

저는 그래서 역순으로 풀었고, min 값을 추적해서 이것보다 arr[i]가 작으면 discount가 안 일어난다고 여겨서 과정을 단순화했습니다만... 

실제 풀이 속도는 그냥 모든 경우를 조사하는 게 더 빨랐습니다. 이건 데이터 품질이랑 관계가 있는 거라 안타깝네요.

```java
class Solution {
    public int[] finalPrices(int[] prices) {
        int[] answer = new int[prices.length];

        int min = prices[prices.length -1];
        answer[prices.length -1] = min;
        for(int i = prices.length - 2; i >= 0; i--){
            if(prices[i] >= min){
                for(int j = i + 1; j< prices.length; j++){
                    if(prices[i] >= prices[j]){
                        answer[i] = prices[i] - prices[j];
                        break;
                    }
                }
                continue;
            }
            answer[i] = prices[i];
            min = prices[i];
        }

        return answer;
    }
}
```

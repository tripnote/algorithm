#문제(https://leetcode.com/problems/group-anagrams/description/)

2024/07/20

중급

걸린 시간: 40분

문제 자체는 그렇게 어렵지 않습니다.
anagram을 어떻게 판별하는 지가 중요한데 
첫 번째는 단어를 sort해서 같은지 비교하는 방식이 있고 알파벳 갯수를 카운트 해서 비교하는 방식이 있습니다. 전자가 나은 방식이라고 생각합니다.

저는 아주 오랜 시간이 걸렸는데.. 바보같이 list형만 고집해서 그렇습니다. map으로 key가 containsKey()로 존재하는지 확인하면 속도가 훨씬 줄어들 겁니다. 
(전에는 모범답안대로 풀었는데 map을 구현해놓고 List와 같은 처리법을 써서 time-exceed가 났었습니다. 모범 답안을 보고서야 깨달았네요...)
시간 복잡도는 O(NM), N은 텍스트의 갯수, K는 텍스트의 최대 길이입니다.

```java
class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> anagrams = new ArrayList<>();


        for(String str: strs){
            if(anagrams.isEmpty()){
                List<String> anagram = new ArrayList<>();
                anagram.add(str);
                anagrams.add(anagram);
                continue;

            }
            boolean isContained = false;

            for(int i = 0; i < anagrams.size(); i++){
                if(isAnagram(anagrams.get(i).get(0), str)){
                    anagrams.get(i).add(str);
                    isContained = true;
                    break;

                }
            }
            if(!isContained){
                List<String> newAnagram = new ArrayList<String>();
                newAnagram.add(str);
                anagrams.add(newAnagram);
            }


            
        }
        return anagrams;
    }

    private boolean isAnagram(String str1, String str2){
        if(str1.length() != str2.length()){
            return false;
        }
        char[] str1Chars = str1.toCharArray();
        char[] str2Chars = str2.toCharArray();
        int[] charCounts = new int[26];
        for(int i = 0; i < str1Chars.length; i++ ){
            charCounts[str1Chars[i]-'a']++;
            charCounts[str2Chars[i]-'a']--;
        }
        for(int count: charCounts){
            if (count !=0){
                return false;
            }
        }

        return true;
    }
}
```
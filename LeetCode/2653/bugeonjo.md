#문제(https://leetcode.com/problems/sliding-subarray-beauty/description/)

2024/07/23

중급

푸는 데 걸린 시간: 2시간 14분

array의 subarray들이 있고 각 subarray에서 n번째로 가장 작은 음수를 찾아내는 문제입니다. 

저는 PriorityQueue를 2개 써서 n번째로 작은 수를 뽑아내는 class를 만들었는데, 

조회하는데는 O(1), 뽑아내고 넣는데는 O(log N)인데, 특정 숫자를 제거하는 건 O(N)이라서 time-exceed 에러가 나더군요.

(나중에 알았지만 이런 경우엔 treeMap을 쓰는 게 더 나았습니다. 그러나 조회하는 경우엔 O(k), 

k는 map의 크기여서 map이 큰 경우엔 여전히 문제가 생길 수 있습니다. 이 경우는 logic을 잘 짜면 해결될 수도 있습니다.) 

그리고 문제를 다시 봤더니 원소들의 값이 -50에서 50으로 제한 되어있는 걸 발견했지요... 

문제를 잘 읽어야겠다는 것을 다시 한 번 확인할 수 있었습니다.


문제 자체는 카운팅 정렬을 이용해서 풀었습니다. 삽입 삭제 조회는 전부 O(1) 이라고 생각하시면 될 것 같습니다. 





```java
class Solution {

    class PriorityQueueWithxSmall{
        int count;
        int x;
        PriorityQueue<Integer> small = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> big = new PriorityQueue<>();

        PriorityQueueWithxSmall(int x){
            this.x = x;
        }
        public int peek(){
            if(count >= x){
                return small.peek();
            }
            return 0;
        }
        public void add(int num){
            if(count < x){
                small.offer(num);
                count++;
                return;
            }
            small.offer(num);
            big.offer(small.poll());
            count++;
        }
        public int poll(){
            if(count <= x){
                count--;
                return small.poll();
            }
            int temp = small.poll();
            small.offer(big.poll());
            count--;
            return temp;
        }

        public boolean remove(int num){
            if(count ==0){
                return false;
            }
            count--;
            if(small.peek() <= num && big.remove(num)){
              
                return true;
            }
            if(small.peek() >= num && small.remove(num)){
                if(count <= x - 1 ){
                    return true;
                }
                small.offer(big.poll());
                return true;
            }
            count++;
            return false;

        }

    }
    public int[] getSubarrayBeauty(int[] nums, int k, int x) {
        // PriorityQueueWithxSmall pq = new PriorityQueueWithxSmall(x);
        // int[] answer = new int[nums.length - k + 1];
        // for(int i =0; i < k; i++){
        //     if(nums[i] < 0){
        //         pq.add(nums[i]);
        //     }
        // }
        // answer[0] = pq.peek();
        // for (int i =k; i < nums.length; i++){
        //     if(nums[i] < 0){
        //         pq.add(nums[i]);
        //     }
        //     if(nums[i - k] < 0){
        //         pq.remove(nums[i - k]);            
        //     }
        //     answer[i - k + 1] = pq.peek();
        // }
        // return answer;
        int[] counts = new int[101];
        for(int i = 0; i < k; i ++){
            counts[nums[i] + 50]++;
        }
        

        int[] answer = new int[nums.length - k + 1];
        answer[0] = findBeauty(counts, x);
        for(int i = k; i < nums.length; i++){
            counts[nums[i - k] + 50]--;
            counts[nums[i] + 50]++;
            answer[i - k + 1] = findBeauty(counts, x);
        }
        return answer;
    }

    private int findBeauty(int[] counts, int x){
        int sum = 0;
        for(int i = 0; i < 50; i++){
            sum += counts[i];
            if(sum >= x){
                return i - 50;
            }
        }
        return 0;
    }

}

```
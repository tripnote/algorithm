#문제(https://leetcode.com/problems/find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance/description/)

2024/07/15

중급

걸린 시간: 10시간

---


그래프 문제 중에서 가장 기초적인 최단 경로의 길이를 구하는 문제를 풀 수 있다면 풀 수 있는 문제입니다. 

저는 공부 하긴 했는데 스스로 풀어보려고 하다가 time-exceed 나서 책을 좀 봤습니다. 

벨만-포드 알고리즘으로 풀이는 했는데 실제 사람들은 플로이드-워셜 알고리즘을 사용했습니다. 

이게 훨씬 쉽긴 한데 증명은 약간 어려우니 참고하시길 바랍니다. 

제 풀이법은 O(EV^2) 정도 시간 복잡도를 가지는데(E: edge 갯수, V: site 갯수) 플로이드-워셜은 O(V^3)으로 조금 더 빠르고, 

특히 복잡한 함수를 사용하지 않는다는 점에서 V가 작은 경우 overhead가 훨씬 적을 것으로 기대됩니다.

```java
class Solution {


    class Edge{
        int to;
        int distance;
        
        public Edge(int to, int distance){
            this.to = to;
            this.distance = distance;
        }

    }

    class EdgeGraph{
        int V;
        List<Edge>[] adjEdges;

        public EdgeGraph(int n, int[][] edges){
            V = n;
            adjEdges = (List<Edge>[]) new ArrayList[V];
            for(int i = 0; i < V; i ++){
                adjEdges[i] = new ArrayList<>();
            }

            for(int[] edge: edges){
                adjEdges[edge[0]].add(new Edge(edge[1], edge[2]));
                adjEdges[edge[1]].add(new Edge(edge[0], edge[2]));
            }
        }

        private int[] findMinDistances(int v){
            int[] distanceTo = new int[V];
            boolean[] isOnQueue = new boolean[V];
            Queue<Integer> queue = new LinkedList<>();


            for(int i = 0; i < V; i++){
                distanceTo[i] = Integer.MAX_VALUE;
            }
            distanceTo[v] = 0;

            queue.add(v);
            isOnQueue[v] = true;
            while(!queue.isEmpty()){
                int from = queue.poll();
                isOnQueue[from] = false;

                for(Edge edge: adjEdges[from]){
                    int to = edge.to;
                    if(distanceTo[to] > distanceTo[from] + edge.distance){
                        distanceTo[to] = distanceTo[from] + edge.distance;
                        if(!isOnQueue[to]){
                            queue.add(to);
                            isOnQueue[to] = true;
                        }                    
                    }
                }
            }

            return distanceTo;


        }

        private int findNeighbors(int v,int distanceThreshold){
            int[] distances = findMinDistances(v);
            int count = -1;
            for(int i = 0; i < V; i++){
                if (distances[i] <= distanceThreshold){
                    count++;
                }
            }
            return count;
        }
    }



    public int findTheCity(int n, int[][] edges, int distanceThreshold) {

        EdgeGraph graph = new EdgeGraph(n, edges);

        int count = Integer.MAX_VALUE;
        int index = 0;
        for(int i = 0; i < n; i ++){
            int temp = graph.findNeighbors(i, distanceThreshold);
            if(count >= temp){
                count = temp;
                index = i;
            }

        }

        return index;
    }

}
```

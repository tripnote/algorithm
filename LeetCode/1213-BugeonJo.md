## 문제(https://leetcode.com/problems/intersection-of-three-sorted-arrays/description/)

2024/07/08

초급

1213. Intersection of Three Sorted Arrays

푸는 데 걸린 시간: 29분

---

엄격하게 오름차순인 세 정수 행렬에 모두 속한 정수들의 List를 구하는 문제입니다. 
해쉬 맵을 이용하는 방법이 있고, 3-pointer를 사용하는 방법이 있는데 저는 후자를 선택했습니다.
만들고 보니 꽤나 비효율적인 코드가 되었고, 답안을 보니 훨씬 더 빠른 코드를 발견했는데,
일단은 반성의 의미로 기존에 만든 코드를 올립니다.

```java
class Solution {
    public List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3) {

        List<Integer> intersects = new ArrayList<>();


        int cursor1 = 0;
        int cursor2 = 0;
        int cursor3 = 0;

        while(true){
            if(arr1[cursor1] == arr2[cursor2] && arr2[cursor2] == arr3[cursor3]){
                intersects.add(arr1[cursor1]);
            }


            if(arr1[cursor1] <= arr2[cursor2] && arr1[cursor1] <= arr3[cursor3]){

                if(cursor1 == arr1.length - 1){
                    return intersects;
                }            
                cursor1++;
                continue;
            }

            if(arr2[cursor2] <= arr1[cursor1] && arr2[cursor2] <= arr3[cursor3]){

                if(cursor2 == arr2.length - 1){
                    return intersects;
                }        
                cursor2++;
                continue;
            }

            if(cursor3 == arr3.length - 1){
                return intersects;
            }        
            cursor3++;
        }

    }
}
```

여기서 속도를 빠르게 하는 두 가지 포인트로
- 세 행렬이 같은 값인 경우 각 cursor를 모두 하나씩 올려야 한다는 점
- 크기 비교를 할 때 세 개를 모두 비교할 필요가 없다는 점
입니다. 후자는 둘 중 하나의 크기만 작아도 크기가 같아지기 위해서는 결국 작은 값을 가진 배열의 커서를 높여야 한다는 점에서 기인합니다.
# 문제(https://leetcode.com/problems/airplane-seat-assignment-probability/)

Airplane Seat Assignment Probability

중급

걸린 시간:  17분

문제 코드는 단 한 줄로도 끝날 수 있지만 수학을 잘 모르면 답을 알기가 어렵습니다.

점화식으로 생각할 때 n+1명의 사람이 있는 경우 첫 번째 사람이 특정 좌석에 앉는 확률은 1/(n+1)입니다.

자기 자신에 앉으면 1의 확률로 n+1번째 사람이 자기 자리에 앉고,

k번째에 앉는 경우 f(n+1-k)의 확률로 n+1번째 사람이 자기 자리에 앉는다는 점을 안다면 다음과 같이 점화식을 짤 수 있습니다.

(k번째 사람이 자기 좌석이 없어도 1번째 사람의 좌석에 앉는 것이 마치 자기 자신의 좌석에 앉는 것과 같다는 점을 이용합니다.)

f(n+1) = 1/(n+1) * (f(n) + f(n-1) + ... + f(1))

답은 간단합니다...

```java
class Solution {
    public double nthPersonGetsNthSeat(int n) {
        if(n == 1){
            return 1.0;
        }
        return 1.0/2;
    }
}
```
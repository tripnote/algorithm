## 문제 (https://school.programmers.co.kr/learn/courses/30/lessons/12909)

---

2024/07/30

Lv2

스택-큐/올바른 괄호

---
### 풀이방법: Stack

Stack을 사용하여 뒤에서 부터 ')'인 경우에는 +1을 '('인 경우에는 -1을 하며

중간에 -1이 나오는 경우 올바른 괄호가 아니기 때문에 false를 반환하고

while문이 종료 된 후 합이 0인 경우에만 true를 반환하고 아닌 경우에는 false를 반환한다.

```java
import java.util.*;

class Solution {
    boolean solution(String s) {
        Stack<Character> str = new Stack<>();

        for(int i = 0; i<s.length(); i++){
            str.push(s.charAt(i));
        }


        int sum = 0;
        while(!str.empty()){
            char c = str.pop();
            if(c == '('){
                sum -= 1;
            }else if(c == ')'){
                sum += 1;
            }
            if(sum == -1){
                return false;
            }
        }

        if(sum == 0){
            return true;
        }else{
            return false;
        }

    }
}
```
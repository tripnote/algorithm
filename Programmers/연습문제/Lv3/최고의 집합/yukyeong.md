## 문제 (https://school.programmers.co.kr/learn/courses/30/lessons/12938)

---

2024/07/10

Lv3

연습문제/최고의 집합

---
### 풀이방법

원소의 개수 n = 3, 원소의 합 s = 10 일때 최고의 집합은 ```{3,3,4}```이다.

```java
1. n = 3, s = 10    s/n = 10/3 = {3}

2. n = 2, s = 8
   {s/n, s/n + 1}
   {3, 4}
```

원소의 개수 n = 5, 원소의 합 s = 10 일때 최고의 집합은 ```{2,2,2,2,2}```이다.

```java
1. n = 5, s = 10   s/n = 10/5 = {2}
2. n = 4, s = 8    s/n = 8/4 = {2}
3. n = 3, s = 6    s/n = 6/3 = {2}
4. n = 2, s = 4
   {s/n, s/n}
   {3, 4}
```

원소의 개수 n = 4, 원소의 합 s = 17 일때 최고의 집합은 ```{4,4,4,5}```이다.

```java
1. n = 4, s = 17   s/n = 17/4 = {4}
2. n = 3, s = 13   s/n = 13/3 = {4}
3. n = 2, s = 9
   {s/n, s/n + 1}
   {4, 5}
```

n이 2가 아닌 경우 n/s를 배열에 추가하고 n--, s-=s/n 로 n과 s의 값을 바꿔준다. 이를 n이 2가 될때까지 반복한다.

n이 2인 경우 짝수인지 홀수인지를 구분하여 ```짝수인 경우 {s/n, s/n}, 홀수인 경우 {s/n, s/n + 1}```을 배열에 추가한다.

```java
import java.util.*;
import java.lang.Math;

class Solution {
    public int[] solution(int n, int s) {

        if(s < n){
            int[] exception = {-1};
            return exception;
        }

        int[] answer = new int[n];

        int cnt = n;
        for(int i=0; i<n; i++){
            if(cnt == 2){

                if(s%2 == 0){
                    answer[n-2] = s/2;
                    answer[n-1] = s/2;
                }else{
                    answer[n-2] = s/2;
                    answer[n-1] = s/2 + 1;
                }
                break;
            }

            answer[i] = s/cnt;
            s -= s/cnt;
            cnt--;
        }

        Arrays.sort(answer);
        return answer;
    }
}
```
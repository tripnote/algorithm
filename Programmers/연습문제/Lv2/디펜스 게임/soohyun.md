2024/07/11
프로그래머스 Level 2
(https://school.programmers.co.kr/learn/courses/30/lessons/138871)
푸는 데 걸린 시간: 14분
해당 문제는 주어진 병사 수(n)와 각 라운드의 적의 수(enemy) 배열을 통해, 가능한 최대 라운드를 해결하는 것입니다. 우선순위 큐(PriorityQueue)를 사용하여 가장 큰 적의 수를 추적하며, 남은 병사 수(n)가 음수가 될 때 한 번에 사용할 수 있는 무적권(k)을 효율적으로 사용하여 문제를 해결합니다. 우선순위 큐를 통해 각 라운드에서 만나는 적의 수를 저장하고, 병사 수가 음수가 될 때 가장 큰 적의 수를 제거하여 병사 수를 복구하고, 이를 반복하여 가능한 최대 라운드를 계산합니다.

다음은 해당 문제의 풀이 코드입니다:

import java.util.*;
class Solution {
public int solution(int n, int k, int[] enemy) {
if(k>=enemy.length)
return enemy.length;
PriorityQueue<Integer> que = new PriorityQueue<>(Collections.reverseOrder());
int answer = 0;
for(int i=0; i<enemy.length; i++){
int e = enemy[i];
n-=e;
que.add(e);
if(n>=0){
answer++;
}else{
if(!que.isEmpty() && k>0){
k--;
n+=que.poll();
answer++;
}else{
break;
}
}
}
return answer;
}
}
2024/07/07
프로그래머스 Level 2
(https://school.programmers.co.kr/learn/courses/30/lessons/178870)
푸는 데 걸린 시간: 21분
해당 문제는 사용자가 새로운 Class를 선언해서 풀이를 진행했습니다. 주어진 수열의 부분 수열 중 조건을 만족하는 것이 여러개 존재할 수 있습니다. 우리가 원하는 것은 길이가 짧은 것 중 시작 인덱스가 가장 작은 것 하나만을 원하므로 새로운 Class를 만들어  Comparable 인터페이스를 상속받아 정렬 조건인 compareTo를 재정의 하였습니다. left, right 두 포인터가 각 배열의 요소를 한 번씩 방문하므로 시간복잡도는 O(n), 부분 배열 저자 및 정렬에서는 O(m * logm)이므로 전체 시간 복잡도는 O(n)입니다.
프로그래머스 스쿨
코딩테스트 연습 - 연속된 부분 수열의 합
이미지
import java.util.*;

class Solution {
public int[] solution(int[] sequence, int k) {
int left = 0;
int right = 0;
int partitionSum = sequence[0];

        int n = sequence.length;

        List<SubSequence> subs = new ArrayList<>();
        while (true){
            if(partitionSum == k){
                subs.add(new SubSequence(left, right));
            }
            if(left == n && right == n) break;

            if(partitionSum <= k && right < n){
                right++;
                if(right < n) partitionSum += sequence[right];
            } else {
                if(left < n) partitionSum -= sequence[left];
                left++;
            }
        }

        Collections.sort(subs);
        return new int[]{subs.get(0).left, subs.get(0).right};
    }

    private class SubSequence implements Comparable<SubSequence>{
        int left, right, size;

        public SubSequence(int left, int right) {
            this.left = left;
            this.right = right;
            this.size = right - left;
        }

        @Override
        public int compareTo(SubSequence o) {
            if(this.size == o.size){
                return this.left - o.left;
            }
            return this.size - o.size;
        }
    }
}
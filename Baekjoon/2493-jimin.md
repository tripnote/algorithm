## 문제(https://www.acmicpc.net/problem/2493)

2024/07/19

골드5

2493. 탑

푸는 데 걸린 시간: 31분

---

스택을 이용해 푸는 문제.

스택 s를 만들어서 최종적으로는 또 다른 탑의 레이저를 받을 수 있는 탑들만 남긴다

1. 먼저 새로운 탑의 높이를 height 변수로 입력받는다.
2. 스택 s의 top과 height를 비교해서 새로운 탑의 높이(height)가 더 크다면 pop한다
3. 2번 과정을 반복해서 스택을 비우거나, height값보다 높은 탑의 높이를 top에 남긴다
4. 스택이 비었다면 0을, 아니라면 해당 탑의 순서를 출력한다.

처음에
ios::sync_with_stdio(0);
cin.tie(0);
를 잊어버리고 제출해서 시간 초과남..ㅠ 잊어버리지 말자...

```c++
#include <bits/stdc++.h>

using namespace std;

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    int n, height;
    stack<int> seq;
    stack<int> s;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> height;
        while (!s.empty()) {
            if (s.top() < height) {
                s.pop();
                seq.pop();
            } else break;
        }
        if (s.empty()) cout << 0 << ' ';
        else cout << seq.top() << ' ';
        s.push(height);
        seq.push(i);
    }
}
```

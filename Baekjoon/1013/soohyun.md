import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main {
    public static void main(String args[]) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());
        String regex="(100+1+|01)+";
        for(int t=0; t<T; t++) {
            String str = br.readLine();
            if(str.matches(regex))
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }    
}
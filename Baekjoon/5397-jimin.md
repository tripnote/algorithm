## 문제(https://www.acmicpc.net/problem/5397)

2024/07/11

실버2

5397. 키로거

푸는 데 걸린 시간: 모름.. 한시간 이내?

---

연결 리스트를 이용해 푸는 문제.
데이터를 중간에 삽입, 제거하는 경우가 많기 때문에 이중 연결리스트인 list를 사용했다. 
list의 반복자인 cursor을 선언해서 연결 리스트를 탐색하였다.

```c++
#include <bits/stdc++.h>

using namespace std;

int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    int testCase;
    cin >> testCase;
    while (testCase--) {
        list<char> li;
        auto cursor = li.end();

        string keyLogger;
        cin >> keyLogger;

        //실제 커서는 cursor가 가리키는 글자 앞에 있다고 생각
        for (auto c: keyLogger) {
            if (c == '<') {
                if (cursor != li.begin()) cursor--;
            } else if (c == '>') {
                if (cursor != li.end()) cursor++;
            } else if (c == '-') {
                if (cursor != li.begin()) cursor = li.erase(--cursor);
            }
            //insert는 cursor가 가리키는 위치에 값 추가
            //그러나 여전히 cursor는 원래 가리키던 값을 가리킴.
            else li.insert(cursor, c);

        }

        for (auto e: li) cout << e;
        cout << '\n';
    }
}
```

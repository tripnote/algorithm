2024/07/12

Silver III2346
풍선 터뜨리기

## 문제(https://www.acmicpc.net/problem/2346)


풍선을 터뜨릴 때마다 그 풍선 안에 들어있던 수의 값만큼 이동하여 다음 풍선을 터뜨리는 방식으로 진행   
이동할 때는 이미 터진 풍선은 제외하고 이동   
원형 리스트의 특성을 고려해서 접근이 필요함   

```java
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] papers = new int[n];

        for (int i = 0; i < n; i++) {
            papers[i] = scanner.nextInt();
        }

        LinkedList<Integer> balloons = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            balloons.add(i + 1);
        }

        int index = 0;
        while (!balloons.isEmpty()) {
            // 현재 풍선의 번호와 그 풍선 안에 적힌 숫자
            int currentBalloon = balloons.remove(index);
            if (!balloons.isEmpty()) {
                System.out.print(currentBalloon + " ");
            }


            if (balloons.isEmpty()) {
                System.out.print(currentBalloon);
                break;
            }

            int move = papers[currentBalloon - 1];
            if (move > 0) {
                index = (index + (move - 1)) % balloons.size();
            } else {
                index = (index + move) % balloons.size();
                if (index < 0) {
                    index += balloons.size();
                }
            }
        }
    }
}
```
# Algorithm

Tripnote 팀원들의 알고리즘 문제 풀이 저장 및 공유

## 파일 및 폴더 구조
- /플랫폼/카테고리(생략가능)/문제(번호)/이름
- /Baekjoon/삼성 SW 역량테스트 기출 문제/13460/정유경.md
- /Programmers/2024 KAKAO WINTER INTERNSHIP/Lv1/가장 많이 받은 선물/정유경.md

## 커밋 규칙

- main에 바로 push
- ```git push origin main```



